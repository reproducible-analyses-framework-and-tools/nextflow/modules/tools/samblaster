#!/usr/bin/env nextflow

process bamblaster {
// Runs samblaster, but on BAM files
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: mkdup_bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.mkdup.bam") - MarkDup Alignment File
//
// require:
//   BAMS
//   params.picard_mark_duplicates_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samblaster_container'
  label 'bamblaster'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/bamblaster", pattern: "*.marked_dup_metrics.txt"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.bamblst.bam"), emit: bamblst_bams

  script:
  """
  set -o pipefail
  samtools view -h -@ ${task.cpus} ${bam} |\
  samtools sort -@ ${task.cpus} -O sam -n |\
  samblaster ${parstr} |\
  samtools sort -@ ${task.cpus} -O bam > ${dataset}-${pat_name}-${run}.bamblst.bam
  """
}
